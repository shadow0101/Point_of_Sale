<?php include("header.php");  ?>
<html>
  <head>
    <title>Customers</title>
    <link rel="import" href="header.php">
    <style>
      {
        background-color: #FFF;border: 1px solid #DDD;
        border-radius: 3px;color: #AAA;
        padding: 20px;margin: 50px auto 0px auto;width: 70%;
      }
      input{
      margin: 0 auto; width: 60%;
      }
      table {
        height: 10%;
        left: 10%;
        margin: 20px auto;
        overflow-y: scroll;
        position: static;
        width: 80%;}
      thead th {
        background: #88CCF1;
        color: #FFF;
        font-family: 'Lato', sans-serif;
        font-size: 16px;
        font-weight: 100;
        letter-spacing: 2px;
        text-transform: uppercase;}
      tr {
        background: #f4f7f8;border-bottom: 1px solid #FFF;
        margin-bottom: 1px;}
      th, td {
        font-family: 'Lato', sans-serif;
        font-weight: 400;
        font-size: 15px;
        padding: 20px;
        text-align: left;
        width: 33.3333%;}
    </style>
  </head>
  <body>
      <p>Search customer:<input type="text" name="search_cus"/></p>
    <?php
      require("config.php");
      $sql="SELECT * FROM Customers";
      $result=mysqli_query($con,$sql);
      echo "<table>
      <thead>
      <tr>
      <th>Customer ID</th>
      <th>Customer Name</th>
      </tr>
      </thead>";
      while($row=mysqli_fetch_array($result)){
        echo "<tr>";
        echo "<td>" . $row['cus_id'] ."</td>";
        echo "<td>" . $row['cus_name'] ."</td>";
        echo "</tr>";
      }
      echo "</table>";
    ?>
  </body>
</html>
